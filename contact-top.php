<!-- header-container start -->
			<div class="header-container">
				<div class="header-top dark ">
					<div class="container">
						<div class="row">
							<div class="col-xs-3 col-sm-6 col-md-9">
								<!-- header-top-first start -->
								<!-- ================ -->
								<div class="header-top-first clearfix">
									<ul class="social-links circle small clearfix hidden-xs">
										<li class="googleplus"><a target="_blank" href="https://plus.google.com/b/117715352965033277015/117715352965033277015/posts/p/pub?pageId=117715352965033277015&hl=en&_ga=1.121565896.1378294797.1443924222"><i class="fa fa-google-plus"></i></a></li>
										<li class="youtube"><a target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube-play"></i></a></li>
										<li class="facebook"><a target="_blank" href="http://www.facebook.com/alamopost2"><i class="fa fa-facebook"></i></a></li>
									</ul>
									<div class="social-links hidden-lg hidden-md hidden-sm circle small">
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i></button>
											<ul class="dropdown-menu dropdown-animation">
												<li class="googleplus"><a target="_blank" href="https://plus.google.com/b/117715352965033277015/117715352965033277015/posts/p/pub?pageId=117715352965033277015&hl=en&_ga=1.121565896.1378294797.1443924222"><i class="fa fa-google-plus"></i></a></li>
												<li class="youtube"><a target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube-play"></i></a></li>
												<li class="facebook"><a target="_blank" href="http://www.facebook.com/alamopost2"><i class="fa fa-facebook"></i></a></li>
												
											</ul>
										</div>
									</div>
									<ul class="list-inline hidden-sm hidden-xs">
										<li><i class="fa fa-map-marker pr-5 pl-10"></i>3518 Fredericksburg Rd San Antonio, TX 78201</li>
										<li><i class="fa fa-phone pr-5 pl-10"></i>+1 210.732.1891</li>
										<li><i class="fa fa-envelope-o pr-5 pl-10"></i> info@alamopost2.org</li>
									</ul>
								</div>
								<!-- header-top-first end -->
							</div>
						</div>
					</div>
				</div>
				<!-- header-top end -->
			</div>
							</div>
						</div>
					</div>
				</div>
				<!-- header-top end -->