<?php include("header.php"); ?>
    <!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					
						<!-- main start -->
						<!-- ================ -->
						<div class="row">

							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Alamo Post 2 Events Blog</h1>
							<div class="separator-2"></div>
							<!-- page-title end -->
							
                            <?php include('blog-grid.php'); ?>

						</div>
						
				</div>
			</section>
			<!-- main-container end -->
<?php 			
include("footer.php");
include("jscripts.php");
?>
