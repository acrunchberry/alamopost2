<?php include("header.php"); ?>
			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="home.php">Home</a></li>
						<li><a href="rentals.php">Hall Rentals</a></li>
						<li class="active">Lounge "The Canteen"</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<?php include("lounge-banner.php"); ?>
			
			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container padding-ver-clear">
				<div class="container pv-40">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-8">
							<h1 class="title">Lounge "The Canteen"</h1>
							<div class="separator-2"></div>
							<p>
							    The Lounge can be set up for your Special Event in many different configurations.
							    We have a lecturn and PA system available as well.
							</p>
							<?php include("rental-form.php"); ?>
						</div>
						<!-- main end -->


						<!-- sidebar start -->
						<!-- ================ -->
						<aside class="col-md-4 col-lg-3 col-lg-offset-1">
							<div class="sidebar">
								<div class="block clearfix">
									<h3 class="title">Hall Details</h3>
									<div class="separator-2"></div>
									<ul class="list margin-clear">
										<li><strong>Capacity: </strong> <span class="text-right">50 Occupants</span></li>
										<li><strong>Deposit: </strong> <span class="text-right">Depends on Event</span></li>
										<li><strong>Events: </strong> <span class="text-right">Baby showers, Birthday Parties, Meetings</span></li>
										<li><strong>Commodities: </strong> <span class="text-right">Co-located bar, Kitchen</span></li>
										
									</ul>
								    <h3>Share This</h3>
									<div class="separator-2"></div>
									<ul class="social-links colored circle small">
										<li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
										<li class="twitter"><a target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
										<li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
										<li class="linkedin"><a target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
						</aside>
						<!-- sidebar end -->
					</div>
				</div>
			</section>
			<!-- main-container end -->


			<!-- section start -->
			<!-- ================ -->
			<section class="section pv-40 clearfix">
				<div class="container">
					<h3>Alamo Post 2 <strong>Halls</strong></h3>
					<div class="row grid-space-10">
						<div class="col-sm-4">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
									<img src="images/rentals/bhall.jpg" alt="Center Hall">
									<a href="center-hall.php" class="overlay-link"><i class="fa fa-link"></i></a>
								</div>
								<div class="body">
									<h3>Kit-Kat Hall</h3>
									<div class="separator"></div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center">
								<div class="overlay-container">
									<img src="images/rentals/ahall.jpg" alt="Walsh Hall">
									<a href="main-hall.php" class="overlay-link"><i class="fa fa-link"></i></a>
								</div>
								<div class="body">
									<h3>Walsh Hall</h3>
									<div class="separator"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- section end -->
<?php
include("footer.php");
include("jscripts.php");
?>
