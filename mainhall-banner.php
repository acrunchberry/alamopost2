<!-- banner start -->
			<!-- ================ -->
			<div class="pv-20 banner light-gray-bg">
				<div class="container clearfix">

					<!-- slideshow start -->
					<!-- ================ -->
					<div class="slideshow">
						
						<!-- slider revolution start -->
						<!-- ================ -->
						<div class="slider-banner-container">
							<div class="slider-banner-boxedwidth-stopped">
								<ul class="slides">
									<!-- slide 1 start -->
									<!-- ================ -->
									<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="500" data-saveperformance="on" data-title="">
									
									<!-- main image -->
									<img src="images/rentals/ahall2.jpg" alt="Walsh Hall" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
									
									<!-- Transparent Background -->
									<div class="tp-caption dark-translucent-bg"
										data-x="center"
										data-y="bottom"
										data-speed="600"
										data-start="0">
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption sfb fadeout text-center large_white"
										data-x="center"
										data-y="110"
										data-speed="500"
										data-start="1000"
										data-easing="easeOutQuad">Walsh Hall
									</div>	

									<!-- LAYER NR. 2 -->
									<div class="tp-caption sfb fadeout text-center large_white tp-resizeme"
										data-x="center"
										data-y="155"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"><div class="separator light"></div>
									</div>	

									<!-- LAYER NR. 3 -->
									<div class="tp-caption sfb fadeout medium_white text-center"
										data-x="center"
										data-y="190"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"
										data-endspeed="600">
									</div>

									</li>
									<!-- slide 1 end -->
									
									<!-- slide 2 start -->
									<!-- ================ -->
									<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="500" data-saveperformance="on" data-title="">
									
									<!-- main image -->
									<img src="images/rentals/ahall.jpg" alt="Walsh Hall" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
									
									<!-- Transparent Background -->
									<div class="tp-caption dark-translucent-bg"
										data-x="center"
										data-y="bottom"
										data-speed="600"
										data-start="0">
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption sfb fadeout text-center large_white"
										data-x="center"
										data-y="110"
										data-speed="500"
										data-start="1000"
										data-easing="easeOutQuad">Walsh Hall
									</div>	

									<!-- LAYER NR. 2 -->
									<div class="tp-caption sfb fadeout text-center large_white tp-resizeme"
										data-x="center"
										data-y="155"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"><div class="separator light"></div>
									</div>	

									<!-- LAYER NR. 3 -->
									<div class="tp-caption sfb fadeout medium_white text-center"
										data-x="center"
										data-y="190"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"
										data-endspeed="600">
									</div>

									</li>
									<!-- slide 2 end -->
									
									<!-- slide 3 start -->
									<!-- ================ -->
									<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="500" data-saveperformance="on" data-title="">
									
									<!-- main image -->
									<img src="images/rentals/ahall3.jpg" alt="Walsh Hall" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
									
									<!-- Transparent Background -->
									<div class="tp-caption dark-translucent-bg"
										data-x="center"
										data-y="bottom"
										data-speed="600"
										data-start="0">
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption sfb fadeout text-center large_white"
										data-x="center"
										data-y="110"
										data-speed="500"
										data-start="1000"
										data-easing="easeOutQuad">Walsh Hall
									</div>	

									<!-- LAYER NR. 2 -->
									<div class="tp-caption sfb fadeout text-center large_white tp-resizeme"
										data-x="center"
										data-y="155"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"><div class="separator light"></div>
									</div>	

									<!-- LAYER NR. 3 -->
									<div class="tp-caption sfb fadeout medium_white text-center"
										data-x="center"
										data-y="190"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"
										data-endspeed="600">
									</div>

									</li>
									<!-- slide 3 end -->
								</ul>
								<div class="tp-bannertimer"></div>
							</div>
						</div>
						<!-- slider revolution end -->

					</div>
					<!-- slideshow end -->

				</div>
			</div>
			<!-- banner end -->
