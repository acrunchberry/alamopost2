                                <div class="col-md-4 col-sm-6 isotope-item app-development">
									<div class="image-box shadow bordered text-center mb-20">
										<div class="overlay-container">
											<img src="images/rentals/ahall.jpg" alt="">
											<div class="overlay-top">
												<div class="text">
													<h3><a href="main-hall.php">Walsh Hall</a></h3>
													<p class="small">Large Hall</p>
												</div>
											</div>
											<div class="overlay-bottom">
												<div class="links">
													<a href="main-hall.php" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 isotope-item app-development">
									<div class="image-box shadow bordered text-center mb-20">
										<div class="overlay-container">
											<img src="images/rentals/bhall.jpg" alt="">
											<div class="overlay-top">
												<div class="text">
													<h3><a href="center-hall.php">Kit-Kat Hall</a></h3>
													<p class="small">Medium Hall</p>
												</div>
											</div>
											<div class="overlay-bottom">
												<div class="links">
													<a href="center-hall.php" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 isotope-item app-development">
									<div class="image-box shadow bordered text-center mb-20">
										<div class="overlay-container">
											<img src="images/rentals/canteen.jpg" alt="">
											<div class="overlay-top">
												<div class="text">
													<h3><a href="lounge.php">Canteen</a></h3>
													<p class="small">Lounge</p>
												</div>
											</div>
											<div class="overlay-bottom">
												<div class="links">
													<a href="lounge.php" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>