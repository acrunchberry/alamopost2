<!-- header start -->
				<!-- classes:  -->
				<!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
				<!-- "dark": dark version of header e.g. class="header dark clearfix" -->
				<!-- "full-width": mandatory class for the full-width menu layout -->
				<!-- "centered": mandatory class for the centered logo layout -->
				<!-- ================ --> 
		<header class="header  fixed clearfix">
					
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<!-- header-left start -->
								<!-- ================ -->
								<div class="header-left clearfix">

									<!-- logo -->
									<div id="logo" class="logo">
										<a href="index.php"><img id="logo_img" src="images/logo_ap2.png" alt="Alamo Post 2"></a>
									</div>

									<!-- name-and-slogan -->
							
									
								</div>
								<!-- header-left end -->

							</div>
							<div class="col-md-9">
					
								<!-- header-right start -->
								<!-- ================ -->
								<div class="header-right clearfix">
									
								<!-- main-navigation start -->
								<!-- classes: -->
								<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
								<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
								<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
								<!-- ================ -->
								<div class="main-navigation  animated with-dropdown-buttons">

									<!-- navbar start -->
									<!-- ================ -->
									<nav class="navbar navbar-default" role="navigation">
										<div class="container-fluid">

											<!-- Toggle get grouped for better mobile display -->
											<div class="navbar-header">
												<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
												
											</div>

											<!-- Collect the nav links, forms, and other content for toggling -->
											<div class="collapse navbar-collapse" id="navbar-collapse-1">
												<!-- main-menu -->
												<ul class="nav navbar-nav ">
													<li class="active mega-menu">
														<a href="home.php" class="" data-toggle="">Home</a>
													</li>
													<!-- mega-menu start -->
													<li class="mega-menu">
														<a href="officers.php" class="" data-toggle="">Officers</a>
													</li>
													<li class="mega-menu dropdown">
														<a href="membership.php"  class="dropdown-toggle" data-toggle="dropdown">Membership Info</a>
														<ul class="dropdown-menu">
															<li>
																<div class="row">
																	<div class="col-lg-8 col-md-9">
																		<h4 class="title">Pages</h4>
																		<div class="row">
																			<div class="col-md-12">
																				<div class="divider"></div>
																				<ul class="menu">
																					<li ><a href="documents.php"><i class="fa fa-angle-right"></i>Post Documents</a></li>
																					<li ><a href="events-blog.php"><i class="fa fa-angle-right"></i>Post Events</a></li>
																					</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</li>
														</ul>
													</li>
													<li class="mega-menu">
														<a href="rentals.php"  class="" data-toggle="">Rentals</a>
													</li>
												</ul>
											</div>	
											
												<!-- header dropdown buttons -->
												<div class="header-dropdown-buttons hidden-xs ">
													<div class="btn-group dropdown">
														<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></button>
														<ul class="dropdown-menu dropdown-menu-right dropdown-animation">
															<li>
																<form role="search" class="search-box margin-clear">
																	<div class="form-group has-feedback">
																		<input type="text" class="form-control" placeholder="Search">
																		<i class="icon-search form-control-feedback"></i>
																	</div>
																</form>
															</li>
														</ul>
													</div>
												</div>

										</div>
									</nav>
									<!-- navbar end -->
								</div>
								<!-- main-navigation end -->	
								</div>
							<!-- header-right end -->
							</div>
						</div>
					</div>
					
		</header>
				<!-- header end -->

		