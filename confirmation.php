<?php
	include("header.php");
?>
<div id="page-start"></div>
					
    <div class="container">
    	<div class="row">
    		<div class="col-md-2"></div>
    		<div class="col-md-8">
    			<div class="alert alert-success" role="alert">
    				<h3>Thanks for joining the Alamo Post 2 Newsletter! </h3>
				</div>
		
					<p class="">
						We will do our best to provide you with updates, news and events regarding the post!
						All members are welcomed to volunteer at the post to help fellow veterans and the post.
					</p>
					<p>
						 We are always looking for Alamo Post 2 members to join our events committee to bring
						 fun and interesting events or workshops to the post.
					</p>
					<p>
						 For all questions regarding events or membership information please contact
						 <strong>info@alamopost2.com</strong> or <strong>commander@alamopost2.com</strong>
					</p>
				</div><!-- end div col-md-8-->
				<div class="col-md-2"></div>
				
				<!-- link back to your Home Page -->
				<div class="col-md-8 col-md-offset-2">
					<a target="_blank" href="https://www.facebook.com/alamopost2"><h5 class="btn btn-primary"> To like our Facebook page, click <span>HERE.</span></h5></a>
					<a target="_blank" href="http://www.alamopost2.com"><h5 class="btn btn-success">To return back to the alamo post 2 home page, click <span> HERE.</span></h5></a>
				</div><!-- end div col-md-8-->
				<div class="separator-2"></div>
		</div><!-- end div row-->
	</div><!-- end div container-->
<?php include('footer.php') ?>
