<?php include "includes/admin_header.php"; ?>
<?php
$query = "SELECT * FROM members";
$select_all_members = mysqli_query($connection, $query);
$member_count = mysqli_num_rows($select_all_members);

?>
<!-- start: Header -->
	
		<div class="container-fluid-full">
		<div class="row-fluid">
				
<?php include "includes/admin_navbar.php"; ?>
	
			<!-- start: Content -->
			<div id="content" class="span10">
			
<!--<?php include "includes/admin_breadcrumbs.php"; ?>-->
			<div class="row-fluid">
				<h3>Welcome,  <?php echo $_SESSION['firstname'] . " " . $_SESSION['lastname'] ?></h3>
				
				<div class="span3 statbox purple" onTablet="span6" onDesktop="span3">
					<!--<div class="boxchart"></div>-->
					<div class="number"><?php echo $member_count ?><i class="icon-arrow-up"></i></div>
					<div class="title">Members</div>
					<div class="footer">
						<a href="members.php"> read full report</a>
					</div>
	            </div>
				<div class="span3 statbox green" onTablet="span6" onDesktop="span3">
					<div class="boxchart">1,2,6,4,0,8,2,4,5,3,1,7,5</div>
					<div class="number">123<i class="icon-arrow-up"></i></div>
					<div class="title">Events</div>
					<div class="footer">
						<a href="#"> read full report</a>
					</div>
				</div>
				<div class="span3 statbox blue noMargin" onTablet="span6" onDesktop="span3">
					<div class="boxchart">5,6,7,2,0,-4,-2,4,8,2,3,3,2</div>
					<div class="number">982<i class="icon-arrow-up"></i></div>
					<div class="title"></div>
					<div class="footer">
						<a href="#"> read full report</a>
					</div>
				</div>
				
				
			</div>		

			<!--<div class="row-fluid">-->
				
			<!--	<div class="span8 widget blue" onTablet="span7" onDesktop="span8">-->
					
			<!--		<div id="stats-chart2"  style="height:282px" ></div>-->
					
			<!--	</div>-->
				
			<!--	<div class="sparkLineStats span4 widget green" onTablet="span5" onDesktop="span4">-->

   <!--                 <ul class="unstyled">-->
                        
   <!--                     <li><span class="sparkLineStats3"></span> -->
   <!--                         Pageviews: -->
   <!--                         <span class="number">781</span>-->
   <!--                     </li>-->
   <!--                     <li><span class="sparkLineStats4"></span>-->
   <!--                         Pages / Visit: -->
   <!--                         <span class="number">2,19</span>-->
   <!--                     </li>-->
   <!--                     <li><span class="sparkLineStats5"></span>-->
   <!--                         Avg. Visit Duration: -->
   <!--                         <span class="number">00:02:58</span>-->
   <!--                     </li>-->
   <!--                     <li><span class="sparkLineStats6"></span>-->
   <!--                         Bounce Rate: <span class="number">59,83%</span>-->
   <!--                     </li>-->
   <!--                     <li><span class="sparkLineStats7"></span>-->
   <!--                         % New Visits: -->
   <!--                         <span class="number">70,79%</span>-->
   <!--                     </li>-->
   <!--                     <li><span class="sparkLineStats8"></span>-->
   <!--                         % Returning Visitor: -->
   <!--                         <span class="number">29,21%</span>-->
   <!--                     </li>-->

   <!--                 </ul>-->
					
			<!--		<div class="clearfix"></div>-->

   <!--             </div><!-- End .sparkStats -->

			<!--</div>-->
			
			<!--<div class="row-fluid">-->
				
			<!--	<div class="box black span4" onTablet="span6" onDesktop="span4">-->
			<!--		<div class="box-header">-->
			<!--			<h2><i class="halflings-icon white list"></i><span class="break"></span>Weekly Stat</h2>-->
			<!--			<div class="box-icon">-->
			<!--				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>-->
			<!--				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>-->
			<!--			</div>-->
			<!--		</div>-->
			<!--		<div class="box-content">-->
			<!--			<ul class="dashboard-list metro">-->
			<!--				<li>-->
			<!--					<a href="#">-->
			<!--						<i class="icon-arrow-up green"></i>                               -->
			<!--						<strong>92</strong>-->
			<!--						New Comments                                    -->
			<!--					</a>-->
			<!--				</li>-->
			<!--			  <li>-->
			<!--				<a href="#">-->
			<!--				  <i class="icon-arrow-down red"></i>-->
			<!--				  <strong>15</strong>-->
			<!--				  New Registrations-->
			<!--				</a>-->
			<!--			  </li>-->
			<!--			  <li>-->
			<!--				<a href="#">-->
			<!--				  <i class="icon-minus blue"></i>-->
			<!--				  <strong>36</strong>-->
			<!--				  New Articles                                    -->
			<!--				</a>-->
			<!--			  </li>-->
			<!--			  <li>-->
			<!--				<a href="#">-->
			<!--				  <i class="icon-comment yellow"></i>-->
			<!--				  <strong>45</strong>-->
			<!--				  User reviews                                    -->
			<!--				</a>-->
			<!--			  </li>-->
			<!--			  <li>-->
			<!--				<a href="#">-->
			<!--				  <i class="icon-arrow-up green"></i>                               -->
			<!--				  <strong>112</strong>-->
			<!--				  New Comments                                    -->
			<!--				</a>-->
			<!--			  </li>-->
			<!--			  <li>-->
			<!--				<a href="#">-->
			<!--				  <i class="icon-arrow-down red"></i>-->
			<!--				  <strong>31</strong>-->
			<!--				  New Registrations-->
			<!--				</a>-->
			<!--			  </li>-->
			<!--			  <li>-->
			<!--				<a href="#">-->
			<!--				  <i class="icon-minus blue"></i>-->
			<!--				  <strong>93</strong>-->
			<!--				  New Articles                                    -->
			<!--				</a>-->
			<!--			  </li>-->
			<!--			  <li>-->
			<!--				<a href="#">-->
			<!--				  <i class="icon-comment yellow"></i>-->
			<!--				  <strong>256</strong>-->
			<!--				  User reviews                                    -->
			<!--				</a>-->
			<!--			  </li>-->
			<!--			</ul>-->
			<!--		</div>-->
			<!--	</div><!--/span-->
				
			
	
<?php include "includes/admin_footer.php"; ?>