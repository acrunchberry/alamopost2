<?php

if(isset($_GET['m_id'])) {

    $the_member_id = $_GET['m_id'];    
    
}

$query = "SELECT * FROM members WHERE member_id = $the_member_id ";
$select_members_by_id = mysqli_query($connection, $query);
    
while($row = mysqli_fetch_assoc($select_members_by_id)) {
    $member_id = $row['member_id'];
    $member_firstname = $row['member_firstname'];
    $member_lastname = $row['member_lastname'];
    $member_number = $row['member_number'];
    $member_email = $row['member_email'];
    $member_phone1 = $row['member_phone1'];
    $member_address = $row['member_address'];
    $member_city = $row['member_city'];
    $member_st = $row['member_st'];
    $member_zip = $row['member_zip'];
    $member_exp_date = $row['member_exp_date'];
    $member_renew_date = $row['member_renew_date'];
    
    }
    
    if(isset($_POST['update_member'])) {
    
    $member_firstname = $_POST['member_firstname'];
    $member_lastname = $_POST['member_lastname'];
    $member_number = $_POST['member_number'];
    $member_email = $_POST['member_email'];
    $member_phone1 = $_POST['member_phone1'];
    $member_address = $_POST['member_address'];
    $member_city = $_POST['member_city'];
    $member_st = $_POST['member_st'];
    $member_zip = $_POST['member_zip'];
    $member_exp_date = $_POST['member_exp_date'];
    $member_renew_date = $_POST['member_renew_date'];
    
    
     $query = "UPDATE members SET ";
    $query .= "member_firstname = '{$member_firstname}', ";
    $query .= "member_lastname = '{$member_lastname}', ";
    $query .= "member_number = '{$member_number}', ";
    $query .= "member_email = '{$member_email}', ";
    $query .= "member_phone1 = '{$member_phone1}', ";
    $query .= "member_address = '{$member_address}', ";
    $query .= "member_city = '{$member_city}', ";
    $query .= "member_st = '{$member_st}', ";
    $query .= "member_zip = '{$member_zip}', ";
    $query .= "member_exp_date = '{$member_exp_date}', ";
    $query .= "member_renew_date = '{$member_renew_date}' ";
    $query .= "WHERE member_id = {$member_id} ";
    
    $update_member = mysqli_query($connection, $query);
    
    confirmQuery($update_member);
        echo "<p class='alert alert-success'>Member Updated. <a href='../members.php?m_id={$the_member_id}'>View Members</a> or <a href='members.php'>Edit more Members</a></p>";
    }
?>

<form action="" method="post" enctype="multipart/form-data">
    
    <div class="form-group">
        <label for="firstname">First Name</label>
            <input value="<?php echo $member_firstname; ?>" type="text" class="form-control" name="member_firstname">
    </div>
    <div class="form-group">
        <label for="lastname">Last Name</label>
            <input value="<?php echo $member_lastname; ?>" type="text" class="form-control" name="member_lastname">
    </div>
    <div class="form-group">
        <label for="member_number">Member ID Number</label>
            <input value="<?php echo $member_number; ?>" type="text" class="form-control" name="member_number">
    </div>
    <div class="form-group">
        <label for="member_email">Contact Email</label>
            <input value="<?php echo $member_email; ?>" type="text" class="form-control" name="member_email">
    </div>
    <div class="form-group">
        <label for="member_phone1">Contact Phone</label>
            <input value="<?php echo $member_phone1; ?>" type="text" class="form-control" name="member_phone1">
    </div>
    <div class="form-group">
        <label for="member_address">Address</label>
            <input value="<?php echo $member_address; ?>" type="text" class="form-control" name="member_address">
    </div>
    <div class="form-group">
        <label for="member_city">City</label>
            <input value="<?php echo $member_city; ?>" type="text" class="form-control" name="member_city">
    </div>
    
    <div class="form-group">
        <label for="member_st">State</label>
        </br>
        <select name="member_st" id="">
            
        <?php
        
        $query = "SELECT * FROM tbl_state";
        $select_state = mysqli_query($connection, $query);
        
        confirmQuery($select_state);
            
        while($row = mysqli_fetch_assoc($select_state )) {
        $state_id = $row['state_id'];
        $state_abbr = $row['state_abbr'];
            
            echo "<option value='$state_id'>{$state_abbr}</option>";
            }
        
        ?>
        </select>
    </div>
    
    <div class="form-group">
        <label for="member_zip">Zipcode</label>
            <input value="<?php echo $member_zip; ?>" type="text" class="form-control" name="member_zip">
    </div>
    <div class="form-group" id=''>
        <label for="member_exp_date">Member Expiration Date</label>
            <input value="<?php echo $member_exp_date; ?>" type='text' class="form-control" name="member_exp_date" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
    </div>
    <div class="form-group" id=''>
        <label for="member_renew_date">Member Renew Date</label>
            <input value="<?php echo $member_renew_date; ?>" type='text' class="form-control" name="member_renew_date" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="update_member" value="Update Member">
    </div>
</form>