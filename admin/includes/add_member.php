<?php

if(isset($_POST['create_member'])) {

$member_firstname = $_POST['member_firstname'];
$member_lastname = $_POST['member_lastname'];
$member_number = $_POST['member_number'];
$member_email = $_POST['member_email'];
$member_phone1 = $_POST['member_phone1'];
$member_address = $_POST['member_address'];
$member_city = $_POST['member_city'];
$member_st = $_POST['member_st'];
$member_zip = $_POST['member_zip'];
$member_exp_date = $_POST['member_exp_date'];
$member_renew_date = $_POST['member_renew_date'];    

$query = "INSERT INTO members(member_firstname, member_lastname, member_number, member_email, member_phone1, member_address, member_city, member_st, member_zip, member_exp_date, member_renew_date) ";
$query .= "VALUES('{$member_firstname}','{$member_lastname}','{$member_number}','{$member_email}','{$member_phone1}','{$member_address}','{$member_city}','{$member_st}','{$member_zip}','{$member_exp_date}', '{$member_renew_date}' ) ";    

$create_member_query = mysqli_query($connection, $query);

confirmQuery($create_member_query);
echo "<p class='bg-success'>Member Added. <a href='../members.php?m_id={$the_member_id}'>View Members</a>";
}

?>

<form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    
    <div class="form-group">
        <label for="firstname">First Name</label>
            <input type="text" class="form-control" name="member_firstname">
    </div>
    <div class="form-group">
        <label for="lastname">Last Name</label>
            <input type="text" class="form-control" name="member_lastname">
    </div>
    <div class="form-group">
        <label for="member_number">Member ID Number</label>
            <input type="text" class="form-control" name="member_number">
    </div>
    <div class="form-group">
        <label for="member_email">Contact Email</label>
            <input type="text" class="form-control" name="member_email">
    </div>
    <div class="form-group">
        <label for="member_phone1">Contact Phone</label>
            <input type="text" class="form-control" name="member_phone1">
    </div>
    <div class="form-group">
        <label for="member_address">Address</label>
            <input type="text" class="form-control" name="member_address">
    </div>
    <div class="form-group">
        <label for="member_city">City</label>
            <input type="text" class="form-control" name="member_city">
    </div>
    
    <div class="form-group">
        <label for="member_st">State</label>
        </br>
        <select name="member_st" id="">
            
        <?php
        
        $query = "SELECT * FROM tbl_state";
        $select_state = mysqli_query($connection, $query);
        
        confirmQuery($select_state);
            
        while($row = mysqli_fetch_assoc($select_state )) {
        $state_id = $row['state_id'];
        $state_abbr = $row['state_abbr'];
            
            echo "<option value='$state_id'>{$state_abbr}</option>";
            }
        
        ?>
        </select>
    </div>
    
    <div class="form-group">
        <label for="member_zip">Zipcode</label>
            <input type="text" class="form-control" name="member_zip">
    </div>
    <div class="form-group" id=''>
        <label for="member_exp_date">Member Expiration Date</label>
            <input type='text' class="form-control" name="member_exp_date" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
    </div>
    <div class="form-group" id=''>
        <label for="member_renew_date">Member Renew Date</label>
            <input type='text' class="form-control" name="member_renew_date" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="create_member" value="Add Member">
    </div>
</form>