 <table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Member #</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>City</th>
            <th>ST</th>
            <th>Zipcode</th>
            <th>Member Expiration</th>
            <th>Member Renew</th>
            <th>Delete</th>
            
        </tr>
    </thead>
    <tbody>
  
    <?php
    
    $query = "SELECT * FROM members";
    $select_members = mysqli_query($connection, $query);
    
    while($row = mysqli_fetch_assoc($select_members)) {
    $member_id = $row['member_id'];
    $member_firstname = $row['member_firstname'];
    $member_lastname = $row['member_lastname'];
    $member_number = $row['member_number'];
    $member_email = $row['member_email'];
    $member_phone1 = $row['member_phone1'];
    $member_address = $row['member_address'];
    $member_city = $row['member_city'];
    $member_st = $row['member_st'];
    $member_zip = $row['member_zip'];
    $member_exp_date = $row['member_exp_date'];
    $member_renew_date = $row['member_renew_date'];
    
    echo "<tr>";
    echo "<td>{$member_id}</td>";
    echo "<td>{$member_firstname}</td>";
    echo "<td>{$member_lastname}</td>";
    echo "<td>{$member_number}</td>";
    echo "<td>{$member_email}</td>";
    echo "<td>{$member_phone1}</td>";
    echo "<td>{$member_address}</td>";
    echo "<td>{$member_city}</td>";
    
    $query = "SELECT * FROM tbl_state WHERE state_id = {$member_st} ";
    $select_state = mysqli_query($connection, $query);
                                     
    while($row = mysqli_fetch_assoc($select_state)) {
    $state_id = $row['state_id'];
    $state_abbr = $row['state_abbr'];
    echo "<td>{$state_abbr}</td>";
    
    }
    
    
    echo "<td>{$member_zip}</td>";
    echo "<td>{$member_exp_date}</td>";
    echo "<td>{$member_renew_date}</td>";
    echo "<td><a href='members.php?source=edit_member&m_id={$member_id}'>Edit</a></td>";
    echo "<td><a href='members.php?delete={$member_id}'>Delete</a></td>";
    echo "</tr>";

    }

?>
                                
</tbody>    
</table>

<?php

if(isset($_GET['delete'])) {
$the_post_id = $_GET['delete'];

$query = "DELETE FROM members WHERE member_id = {$the_member_id} ";
$delete_query = mysqli_query($connection, $query);
header("Location: members.php");
} 
?>