<div id="page-start"></div>
		<hr>
			<!-- section start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h2 class="text-center"><strong>General Membership Meetings</strong></h2>
							<div class="separator"></div>
							<p class="large text-center">Please join us for our monthly meeting on the 1st Wednesday of every month @ 6:30pm <br>
								<span class="text-danger"><strong>FREE WIFI FOR OUR MEMBERS &amp; GUESTS</strong></span>
							</p>	
						</div>
						<div class="col-md-4 ">
							<div class="pv-30 ph-20 feature-box bordered shadow text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
								<span class="icon default-bg circle"><i class="fa fa-graduation-cap"></i></span>
								<h3>Legion Uniform Caps</h3>
									<a class="btn btn-warning" href="http://emblem.legion.org/help_uniform_cap.asp">Buy Your Cap Here!</a>
								<div class="separator clearfix"></div>
									<p>Click here for information on cap etiquette and purchasing an American Legion Cap</p>
									<p style="color:#f00">Please follow San Antonio, Texas Guidelines when ordering your Cap.</p>
							</div>
						</div>
						<div class="col-md-4 ">
							<div class="pv-30 ph-20 feature-box bordered shadow text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="150">
								<?php include('calendar.php') ?>
							</div>
						</div>
						<div class="col-md-4 ">
							<div class="pv-30 ph-20 feature-box bordered shadow text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="200">
								<span class="icon default-bg circle"><i class="fa fa-money"></i></span>
								<h3>Donate to the Post</h3>
								<div class="progress">
  									<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
  										<span class="sr-only">0% Complete</span>
  									</div>
								</div>
								<div class="separator clearfix"></div>
									<p>
										Help the post improve our infrastructure and services for our members and fellow veterans. All donations are used to help maintain programs
										that benefit homeless veterans, veteran mental health services, and services available to our members.	
									</p>
									<a class="btn btn-success" href="https://www.gofundme.com/alamopost2">Donate!</a>
									
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- section end -->
