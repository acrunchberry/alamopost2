			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<footer id="footer" class="clearfix ">

				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-3">
									<div class="footer-content">
										<div class="logo-footer"><img id="logo-footer" src="images/logo_ap2.png" alt=""></div>
										<p class='footer-content'>
										We are a non-profit veteran oriented organization that has been a part of the San Antonio
										military community for over 20 years! We serve the San Antonio community through our charitable
										fundraising events that focus on helping our fellow veterans and future leaders of our community...
										<a href="page-about.php">Learn More<i class="fa fa-long-arrow-right pl-5"></i></a>
										</p>
										<div class="separator-2"></div>
										<ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="http://www.facebook.com/alamopost2"><i class="fa fa-facebook"></i></a></li>
											<li class="youtube"><a target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube-play"></i></a></li>
											<li class="googleplus"><a target="_blank" href="https://plus.google.com/b/117715352965033277015/117715352965033277015/posts/p/pub?pageId=117715352965033277015&hl=en&_ga=1.121565896.1378294797.1443924222"><i class="fa fa-google-plus"></i></a></li>
											
										</ul>
										<div class="separator-2"></div>
										<ul class="list-icons">
											<li><i class="fa fa-map-marker pr-10 text-default"></i> 3518 Fredericksburg Rd San Antonio, TX 78201 </li>
											<li><i class="fa fa-phone pr-10 text-default"></i>1(210)732-1891</li>
											<li><i class="fa fa-envelope-o pr-10"></i>info@alamopost2.org</li>
										</ul>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h2 class="title">Latest From Facebook</h2>
										<div class="separator-2"></div>
											<div class="fb-page" data-href="https://www.facebook.com/alamopost2" data-height="415" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true">
												<div class="fb-xfbml-parse-ignore">
													<blockquote cite="https://www.facebook.com/alamopost2">
														<a href="https://www.facebook.com/alamopost2">American Legion Alamo Post 2</a>
													</blockquote>
												</div>
											</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h2 class="title">Subscribe to Newsletter!</h2>
										<div class="separator-2"></div>
											<?php include('newsletterinfo.php') ?>
                    				</div>
                    			</div>	
								<div class="col-md-3">
									<div class="footer-content">
										<h2 class="title">Find Us</h2>
										<div class="separator-2"></div>
											<div class="map">
												<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1736.6754797143244!2d-98.53749481534338!3d29.47695724464389!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x865c5e50f5136971%3A0x1bb144a442d54c22!2sAmerican+Legion!5e0!3m2!1sen!2sus!4v1443679223607" width="310" height="300" frameborder="0" style="border:0" allowfullscreen>
												</iframe>
											</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter">
					<div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center">&copy; American Legion Alamo Post #2 <?php echo date("Y") ?> All right reserved.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .subfooter end -->

			</footer>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->