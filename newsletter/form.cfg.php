<?php exit(0); ?> { 
"settings":
{
	"data_settings" : 
	{
		"save_database" : 
		{
			"database" : "",
			"is_present" : false,
			"password" : "",
			"port" : 3306,
			"server" : "",
			"tablename" : "",
			"username" : ""
		},
		"save_file" : 
		{
			"filename" : "form-results.csv",
			"is_present" : false
		},
		"save_sqlite" : 
		{
			"database" : "newsletter.dat",
			"is_present" : false,
			"tablename" : "newsletter"
		}
	},
	"email_settings" : 
	{
		"auto_response_message" : 
		{
			"custom" : 
			{
				"body" : "<!DOCTYPE html>\n<html dir=\"ltr\" lang=\"en\">\n<head><title>You got mail!</title></head>\n<body style=\"background-color: #f9f9f9; padding-left: 11%; padding-top: 7%; padding-right: 2%; max-width: 700px; font-family: Helvetica, Arial;\">\n<style type=\"text/css\">\nbody {background-color: #f9f9f9;padding-left: 110px;padding-top: 70px; padding-right: 20px;max-width:700px;font-family: Helvetica, Arial;}\np{font-size: 12px; color: #666666;}\nh2{font-size: 28px !important;color: #666666 ! important;margin: 0px; border-bottom: 1px dotted #00A2FF; padding-bottom:3px;}\ntable{width:80%;}\ntd {font-size: 12px !important; line-height: 30px;color: #666666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\ntd:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:10%; padding-right:5px;}\na:link {color:#666666; text-decoration:underline;} a:visited {color:#666666; text-decoration:none;} a:hover {color:#00A2FF;}\nb{font-weight: bold;}\n</style>\n<h2 style=\"font-size: 28px !important;color: #666666 ! important;margin: 0px; border-bottom: 1px dotted #00A2FF; padding-bottom:3px;\">Thanks for taking the time to fill out the form. <br/>Here's a copy of what you submitted:</h2>\n<div>\n[_form_results_]\n</div>\n</body>\n</html>\n",
				"is_present" : true,
				"subject" : "Thank you for your submission"
			},
			"from" : "noreply@alamopost2.com",
			"is_present" : false,
			"to" : ""
		},
		"notification_message" : 
		{
			"bcc" : "info@alamopost2.com",
			"cc" : "commander@alamopost2.com",
			"custom" : 
			{
				"body" : "<!DOCTYPE html>\n<html dir=\"ltr\" lang=\"en\">\n<head><title>New Newsletter Subscription</title></head>\n<body style=\"background-color: #f9f9f9; padding-left: 11%; padding-top: 7%; padding-right: 20px; max-width: 700px; font-family: Helvetica, Arial;\">\n<style type=\"text/css\">\nbody {background-color: #f9f9f9;padding-left: 11%; padding-top: 7%; padding-right: 2%;max-width:700px;font-family: Helvetica, Arial;}\np{font-size: 12px; color: #666666;}\nh1{font-size: 60px !important;color: #cccccc !important;margin:0px;}\nh2{font-size: 28px !important;color: #666666 ! important;margin: 0px; border-bottom: 1px dotted #00A2FF; padding-bottom:3px;}\ntable{width:80%;}\ntd {font-size: 12px !important; line-height: 30px;color: #666666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\ntd:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:10%; padding-right:5px;}\na:link {color:#666666; text-decoration:underline;} a:visited {color:#666666; text-decoration:none;} a:hover {color:#00A2FF;}\nb{font-weight: bold;}\n</style>\n<h1 style=\"font-size: 60px !important; color: #cccccc !important; margin: 0px;\">Hey there,</h1>\n<p style=\"font-size: 12px; color: #666666;\">\nSomeone subscribed to the newsletter, here is their info:\n</p>\n<div>\n[_form_results_]\n</div>\n</body>\n</html>\n",
				"is_present" : true,
				"subject" : "Someone Subscribed to the Newsletter"
			},
			"from" : "newsletter@alamopost2.com",
			"is_present" : true,
			"replyto" : "",
			"to" : "subscribe@alamopost2.com"
		}
	},
	"general_settings" : 
	{
		"colorboxautoenabled" : false,
		"colorboxautotime" : 3,
		"colorboxenabled" : false,
		"colorboxname" : "Default",
		"formname" : "e-news",
		"is_appstore" : "0",
		"timezone" : "UTC"
	},
	"mailchimp" : 
	{
		"apiKey" : "ab6e99312cd3941ea7e8cfd447bc8f6a-us10",
		"lists" : 
		[
			
			{
				"action" : 
				{
					"subscribe" : 
					{
						"condition" : "always"
					},
					"unsubscribe" : 
					{
						"condition" : "never"
					}
				},
				"is_present" : true,
				"listid" : "f8e2d37f87",
				"merge_tags" : 
				[
					
					{
						"fb_name" : "email20",
						"field_type" : "email",
						"isGrouping" : false,
						"name" : "Email Address",
						"req" : true,
						"tag" : "EMAIL"
					},
					
					{
						"fb_name" : "number21",
						"field_type" : "number",
						"isGrouping" : false,
						"name" : "Membership #",
						"req" : false,
						"tag" : "MMERGE3"
					},
					
					{
						"fb_name" : "text22",
						"field_type" : "text",
						"isGrouping" : false,
						"name" : "First Name",
						"req" : false,
						"tag" : "FNAME"
					},
					
					{
						"fb_name" : "text23",
						"field_type" : "text",
						"isGrouping" : false,
						"name" : "Last Name",
						"req" : false,
						"tag" : "LNAME"
					}
				],
				"name" : "Website Signup Form",
				"subscribe" : 
				{
					"double_optin" : false,
					"email_address_field" : "email20",
					"email_type_field" : "html",
					"replace_interests" : true,
					"send_welcome" : false,
					"update_existing" : true
				},
				"unsubscribe" : 
				{
					"delete_member" : false,
					"email_address_field" : "email20",
					"send_goodbye" : true,
					"send_notify" : true
				}
			}
		]
	},
	"payment_settings" : 
	{
		"confirmpayment" : "<center>\n<style type=\"text/css\">\n#docContainer table {width:80%; margin-top: 5px; margin-bottom: 5px;}\n#docContainer td {text-align:right; min-width:25%; font-size: 12px !important; line-height: 20px;margin: 0px;border-bottom: 1px solid #e9e9e9; padding-right:5px;}\n#docContainer td:first-child {text-align:left; font-size: 13px !important; font-weight:bold; vertical-align:text-top; min-width:50%;}\n#docContainer th {font-size: 13px !important; font-weight:bold; vertical-align:text-top; text-align:right; padding-right:5px;}\n#docContainer th:first-child {text-align:left;}\n#docContainer tr:first-child {border-bottom-width:2px; border-bottom-style:solid;}\n#docContainer center {margin-bottom:15px;}\n#docContainer form input { margin:5px; }\n#docContainer #fb_confirm_inline { margin:5px; text-align:center;}\n#docContainer #fb_confirm_inline>center h2 { }\n#docContainer #fb_confirm_inline>center p { margin:5px; }\n#docContainer #fb_confirm_inline>center a { }\n#docContainer #fb_confirm_inline input { border:none; color:transparent; font-size:0px; background-color: transparent; background-repat: no-repeat; }\n#docContainer #fb_paypalwps { background: url('https://coffeecupimages.s3.amazonaws.com/paypal.gif');background-repeat:no-repeat; width:145px; height:42px; }\n#docContainer #fb_authnet { background: url('https://coffeecupimages.s3.amazonaws.com/authnet.gif'); background-repeat:no-repeat; width:135px; height:38px; }\n#docContainer #fb_2checkout { background: url('https://coffeecupimages.s3.amazonaws.com/2co.png'); background-repeat:no-repeat; width:210px; height:44px; }\n#docContainer #fb_invoice { background: url('https://coffeecupimages.s3.amazonaws.com/btn_email.png'); background-repeat:no-repeat; width:102px; height:31px; }\n#docContainer #fb_invoice:hover { background: url('https://coffeecupimages.s3.amazonaws.com/btn_email_hov.png'); }\n#docContainer #fb_goback { color: inherit; }\n</style>\n[_cart_summary_]\n<h2>Almost done! </h2>\n<p>Your order will not be processed until you click the payment button below.</p>\n<a id=\"fb_goback\"href=\"?action=back\">Back to form</a></center>",
		"currencysymbol" : "$",
		"decimals" : 2,
		"fixedprice" : "000",
		"invoicelabel" : "",
		"is_present" : false,
		"paymenttype" : "redirect",
		"shopcurrency" : "USD",
		"usecustomsymbol" : false
	},
	"redirect_settings" : 
	{
		"confirmpage" : "<?php include('news-header.php') ?>\n<?php include('navbar.php') ?>\n\t\t\t\t\t<li><a href=\"home\">Home</a><i class=\"icon-angle-right\"></i></li>\n                </ul>\n            </div>\n        </div>\n    </div>\n    </section>\n    <!-- Page Content -->\n    <div class=\"container\">\n    \t<div class=\"row\">\n    \t\t<div class=\"col-md-2\"></div>\n    \t\t<div class=\"col-md-8\">\n    \t\t\t<div class=\"alert alert-success\" role=\"alert\">\n    \t\t\t\t<h3>Thanks for joining the Alamo Post 2 Newsletter! </h3>\n\t\t\t\t</div>\n\t\t\n\t\t\t\t\t<p class=\"\">\n\t\t\t\t\t\tWe will do our best to provide you with updates, news and events regarding the post!\n\t\t\t\t\t\tAll members are welcomed to volunteer at the post to help fellow veterans and the post.\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\t We are always looking for Alamo Post 2 members to join our events committee to bring\n\t\t\t\t\t\t fun and interesting events or workshops to the post.\n\t\t\t\t\t</p>\n\t\t\t\t\t<p>\n\t\t\t\t\t\t For all questions regarding events or membership information please contact\n\t\t\t\t\t\t <strong>info@alamopost2.com</strong> or <strong>commander@alamopost2.com</strong>\n\t\t\t\t\t</p>\n\t\t\t\t</div><!-- end div col-md-8-->\n\t\t\t\t<div class=\"col-md-2\"></div>\n\t\t\t\t\n\t\t\t\t<!-- link back to your Home Page -->\n\t\t\t\t<div class=\"col-md-4\"></div>\n\t\t\t\t<div class=\"col-md-8\">\n\t\t\t\t\t<h5> To like our Facebook page, click <span> <a target=\"_blank\" href=\"https://www.facebook.com/alamopost2\">HERE. </a></span></h5>\n\t\t\t\t\t<h5>To return back to the alamo post 2 home page, click <span> <a target=\"_blank\" href=\"http://www.alamopost2.com\">HERE.</a></span></h5>\n\t\t\t\t</div><!-- end div col-md-8-->\n\t\t\t\t<div class=\"col-md-2\"></div>\n\t\t</div><!-- end div row-->\n\t</div><!-- end div container-->\n<?php include('news-footer.php') ?>[_form_results_][_submitted_][_form_results_]",
		"gotopage" : "http://www.alamopost2.com/confirmation.php",
		"inline" : "<center>\n<style type=\"text/css\">\n#docContainer table {margin-top: 30px; margin-bottom: 30px; width:80%;}\n#docContainer td {font-size: 12px !important; line-height: 30px;color: #666666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\n#docContainer td:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:50%; padding-right:5px;}\n</style>\n[_form_results_]\n<h2>Thank you for joining Alamo Post 2 E-Newsletter!</h2><br/>\n<p>We'll do our best to keep you updated with new events and information regarding the post activities.</p>\n</center>",
		"type" : "gotopage"
	},
	"uid" : "28ff5ce25af95457b5b9fe52435d6b8a",
	"validation_report" : "in_line"
},
"rules":{"email20":{"email":true,"label":"Email Address","fieldtype":"email","required":true},"number21":{"decimals":0,"range":{"0":0,"1":999999999,"2":1},"label":"Membership #","fieldtype":"number"},"text22":{"label":"First Name","fieldtype":"text"},"text23":{"label":"Last Name","fieldtype":"text"}},
"payment_rules":{},
"conditional_rules":{},
"application_version":"Web Form Builder (Windows), build 2.4.5318"
}