	<!-- banner start -->
			<!-- ================ -->
			<div class="pv-40 banner">
				<div class="container clearfix">

					<!-- slideshow start -->
					<!-- ================ -->
					<div class="slideshow">
						
						<!-- slider revolution start -->
						<!-- ================ -->
						<div class="slider-banner-container">
							<div class="slider-banner-boxedwidth">
								<ul class="slides">
									<!-- slide 1 start -->
									<!-- ================ -->
									<li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Alamo Post 2">
									
									<!-- main image -->
									<img src="images/legion-banner.jpg" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
									
									<!-- Transparent Background -->
									<div class="tp-caption dark-translucent-bg"
										data-x="center"
										data-y="bottom"
										data-speed="600"
										data-start="0">
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption sfb fadeout text-center large_white"
										data-x="center"
										data-y="110"
										data-speed="500"
										data-start="1000"
										data-easing="easeOutQuad">Alamo Post 2
									</div>	

									<!-- LAYER NR. 2 -->
									<div class="tp-caption sfb fadeout text-center large_white tp-resizeme hidden-xs"
										data-x="center"
										data-y="155"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"><div class="separator light"></div>
									</div>	

									<!-- LAYER NR. 3 -->
									<div class="tp-caption sfb fadeout medium_white text-center hidden-xs"
										data-x="center"
										data-y="190"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"
										data-endspeed="600">
										We are a non-profit veterans oriented organization that has been a part<br />
										of the San Antonio military community for over 20 years! We serve the San Antonio community<br />
										through our charitable fundraising events that focus on helping our fellow veterans and future<br />
										leaders of our community.
										
									</div>

									<!-- LAYER NR. 4 
									<div class="tp-caption sfb fadeout small_white text-center hidden-xs"
										data-x="center"
										data-y="300"
										data-speed="500"
										data-start="1600"
										data-easing="easeOutQuad"
										data-endspeed="600"><a href="#" class="btn btn-dark btn-animated">Read More <i class="fa fa-arrow-right"></i></a> <span class="pl-5 pr-5">or</span> <a href="page-contact.html" class="btn btn-default btn-animated">Contact Us <i class="fa fa-envelope"></i></a>
									</div>-->

									</li>
									<!-- slide 1 end -->

									<!-- slide 2 start -->
									<!-- ================ -->
									<li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Entities">
									
									<!-- main image -->
									<img src="images/legion_family_emblem.jpg" alt="slidebg2" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">

									<!-- Transparent Background -->
									<div class="tp-caption dark-translucent-bg"
										data-x="center"
										data-y="bottom"
										data-speed="600"
										data-start="0">
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption sfb fadeout text-center large_white"
										data-x="center"
										data-y="110"
										data-speed="500"
										data-start="1000"
										data-easing="easeOutQuad">Alamo Post 2 Entities
									</div>	

									<!-- LAYER NR. 2 -->
									<div class="tp-caption sfb fadeout text-center large_white tp-resizeme hidden-xs"
										data-x="center"
										data-y="155"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"><div class="separator light"></div>
									</div>	

									<!-- LAYER NR. 3 -->
									<div class="tp-caption sfb fadeout medium_white text-center hidden-xs"
										data-x="center"
										data-y="190"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"
										data-endspeed="600">
										Alamo Post 2 has several entities such as<br>
										The Legion Riders, The Women's Auxillary, Sons of The American Legion<br>
										that each contribute to the betterment of the post, veterans, and community.
										
									</div>

									<!-- LAYER NR. 4 
									<div class="tp-caption sfb fadeout small_white text-center hidden-xs"
										data-x="center"
										data-y="300"
										data-speed="500"
										data-start="1600"
										data-easing="easeOutQuad"
										data-endspeed="600"><a href="#" class="btn btn-dark btn-animated">Read More <i class="fa fa-arrow-right"></i></a> <span class="pl-5 pr-5">or</span> <a href="page-contact.html" class="btn btn-default btn-animated">Contact Us <i class="fa fa-envelope"></i></a>
									</div>-->

									</li>
									<!-- slide 2 end -->
									
									<!-- slide 3 start -->
									<!-- ================ -->
									<li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="General Membership">
									
									<!-- main image -->
									<img src="images/apfront.jpg" alt="slidebg2" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">

									<!-- Transparent Background -->
									<div class="tp-caption dark-translucent-bg"
										data-x="center"
										data-y="bottom"
										data-speed="600"
										data-start="0">
									</div>

									<!-- LAYER NR. 1 -->
									<div class="tp-caption sfb fadeout text-center large_white"
										data-x="center"
										data-y="110"
										data-speed="500"
										data-start="1000"
										data-easing="easeOutQuad">Become a Member. Join the movement.
									</div>	

									<!-- LAYER NR. 2 -->
									<div class="tp-caption sfb fadeout text-center large_white tp-resizeme hidden-xs"
										data-x="center"
										data-y="155"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"><div class="separator light"></div>
									</div>	

									<!-- LAYER NR. 3 -->
									<div class="tp-caption sfb fadeout medium_white text-center hidden-xs"
										data-x="center"
										data-y="190"
										data-speed="500"
										data-start="1300"
										data-easing="easeOutQuad"
										data-endspeed="600">
										Alamo Post 2 contiues to be the model post in San Antonio<br>
										for events and services provided to the community and aiding fellow veterans<br>
										Join us during our Monthly General Membership meetings on the 1st Wednesday of each month
										
									</div>

									<!-- LAYER NR. 4 
									<div class="tp-caption sfb fadeout small_white text-center hidden-xs"
										data-x="center"
										data-y="300"
										data-speed="500"
										data-start="1600"
										data-easing="easeOutQuad"
										data-endspeed="600"><a href="#" class="btn btn-dark btn-animated">Read More <i class="fa fa-arrow-right"></i></a> <span class="pl-5 pr-5">or</span> <a href="page-contact.html" class="btn btn-default btn-animated">Contact Us <i class="fa fa-envelope"></i></a>
									</div>-->

									</li>
									<!-- slide 3 end -->
								</ul>
								<div class="tp-bannertimer"></div>
							</div>
						</div>
						<!-- slider revolution end -->

					</div>
					<!-- slideshow end -->

				</div>
			</div>
			<!-- banner end -->