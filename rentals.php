<?php
include("header.php");
?>
<div id="page-start"></div>
			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="home.php">Home</a></li>
						<li class="active">Alamo Post Halls</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->
			
            <!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">
			<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">

							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Alamo Post 2 Halls</h1>
							<div class="separator-2"></div>
							<!-- page-title end -->
							<p class="lead">Hold your special event at Alamo Post 2. We have two different rooms available.
							    One of them is sure to meet your needs. The Hall is the larger room, but the Lounge can be the
							    perfect room for a number of groups. We have a lot of parking at Alamo Post 2 as well.</p>		    
				            <!-- Start Halls Grid -->
				            
				            <?php
				                include("hall-grid.php");
				            ?>
						</div>
                    </div>
                </div>
        	</section>	

<!-- main-container end -->

			
<?php
include("footer.php");
include("jscripts.php");
?>

