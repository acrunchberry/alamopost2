<?php include("header.php"); ?>


<div id="page-start"></div>
			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="home.php">Home</a></li>
						<li class="active">Alamo Post 2 Officers</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">

							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Elected Officers</h1>
							<div class="separator-2"></div>
							<!-- page-title end -->
							<div class="row grid-space-0">
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/aescobar.jpg" alt="">
											<a href="images/officers/aescobar.jpg" class="popup-img overlay-link" title="Arturo 'Art' Escobar - Commander"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Arturo 'Art' Escobar</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Post Commander <br>commander@alalmopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/al-default.jpg" alt="">
											<a href="images/officers/al-default.jpg" class="popup-img overlay-link" title="Vacant - 1st Vice Commander"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Vacant</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>1st Vice Commander <br>vicecommander@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/roria.jpg" alt="">
											<a href="images/officers/roria.jpg" class="popup-img overlay-link" title="Ricardo Oria - 2nd Vice Commander"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Ricardo Oria</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>2nd Vice Commander <br>vicecommander2@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/jduarte.jpg" alt="">
											<a href="images/officers/jduarte.jpg" class="popup-img overlay-link" title="Joe Duarte - Judge Advocate"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Joe Duarte</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Judge Advocate <br>jag@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row grid-space-0">
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/cvela.jpg" alt="">
											<a href="images/officers/cvela.jpg" class="popup-img overlay-link" title="Carlos Vela - Adjutant"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Carlos Vela</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Adjutant<br>adjutant@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/al-default.jpg" alt="">
											<a href="images/officers/al-default.jpg" class="popup-img overlay-link" title="Israel Adame - Finance Officer"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Israel Adame</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Finance Officer<br> finance@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/bgutierrez.jpg" alt="">
											<a href="images/officers/bgutierrez.jpg" class="popup-img overlay-link" title="'Brig' Gutierrez - Historian"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">'Brig' Gutierrez</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Historian <br> historian@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/scarpenter.jpg" alt="">
											<a href="images/officers/scarpenter.jpg" class="popup-img overlay-link" title="Sameul Carpenter - Chaplain"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Samuel Carpenter</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Chaplain <br> chaplain@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row grid-space-0">
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/aswezey.jpg" alt="">
											<a href="images/officers/aswezey.jpg" class="popup-img overlay-link" title="Art Swezey - Service Officer"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Art Swezey</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Service Officer <br>service@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/amarin.jpg" alt="">
											<a href="images/officers/amarin.jpg" class="popup-img overlay-link" title="Arnold Marin - Sgt-at-Arms"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Arnold Marin</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Sgt-at-Arms <br>sgtatarms@alamopost2.com</em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/al-default.jpg" alt="">
											<a href="images/officers/al-default.jpg" class="popup-img overlay-link" title="Johnny Hernandez - Executive Officer"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Johnny Hernandez</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Executive Officer</em> <br> </em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/jgarza.jpg" alt="">
											<a href="images/officers/jgarza.jpg" class="popup-img overlay-link" title="Joe Garza - Executive Officer"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Joe Garza</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Executive Officer <br> </em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/porkchop.jpg" alt="">
											<a href="images/officers/porkchop.jpg" class="popup-img overlay-link" title="Dan 'Porkchop' Rodriguez - Executive Officer"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Dan "Porkchop" Rodriguez</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Executive Officer <br> </em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/lombrano.jpg" alt="">
											<a href="images/officers/lombrano.jpg" class="popup-img overlay-link" title="John Lombrano - Executive Officer"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">John Lombrano</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Executive Officer <br> </em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="image-box team-member shadow mb-20">
										<div class="overlay-container overlay-visible">
											<img src="images/officers/hfleisher.jpg" alt="">
											<a href="images/officers/hfleisher.jpg" class="popup-img overlay-link" title="Harold Fleischer, III - Executive Officer"><i class="icon-plus-1"></i></a>
											<div class="overlay-bottom">
												<div class="text">
													<h3 class="title">Harold Fleischer, III</h3>
													<div class="separator light"></div>
													<p class="small margin-clear"><em>Executive Officer <br> </em></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							</div>
						</div>
						<!-- main end -->
							
					</div>
			</section>
			<!-- main-container end -->

			
<?php
include("footer.php");
include("jscripts.php");
?>