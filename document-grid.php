                                <div class="image-box style-3-b">
									<div class="row">
										<div class="col-sm-6 col-md-4 col-lg-3">
										    <div class="overlay-container">
										        <img src="images/documents.png" alt="">
										        <div class="overlay-to-top">
										            <p class="small margin-clear"><em>Alamo Post 2 Constitution &amp; By-Laws</em></p>    
										        </div>
										    </div>
										</div>    
										<div class="col-sm-6 col-md-8 col-lg-9">
    									    <div class="body">
    											<h3 class="title"><em>Alamo Post 2 Constitution &amp; By-Laws</em></h3>
    											<p class="small mb-10"><i class="icon-calendar"></i> Jan, 2014 <i class="pl-10 icon-tag-1"></i> Forms</p>
    										    	<div class="separator-2"></div>
    											<p class="mb-10">Alamo Post 2 Constitution &amp; By-Laws which were edited and voted upon for approval by the Executive Board and General Membership of Alamo 
    											Post 2. Each year the Constituion &amp; By-Laws are presented
    											to the General Membership body for review and updates or revisions are applied.</p>
    											<form method="get" action="/documents/alamo-post-2-c&bl.pdf">
    												<button class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear" type="submit">Download!<i class="fa fa-arrow-right pl-10"></i></button>
												</form>
    										</div>
										</div>
									</div>
								</div>
								
								<div class="image-box style-3-b">
									<div class="row">
										<div class="col-sm-6 col-md-4 col-lg-3">
										    <div class="overlay-container">
										        <img src="images/documents.png" alt="">
										        <div class="overlay-to-top">
										            <p class="small margin-clear"><em>Robert's Rules of Order</em></p>    
										        </div>
										    </div>
										</div>    
										<div class="col-sm-6 col-md-8 col-lg-9">
    									    <div class="body">
    											<h3 class="title"><em>Robert's Rules of Order</em></h3>
    											<p class="small mb-10"><i class="icon-calendar"></i> Jan, 2015 <i class="pl-10 icon-tag-1"></i> Forms</p>
    										    	<div class="separator-2"></div>
    											<p class="mb-10">Robert's Rules of Order is the short title of a book, written by Brig. Gen. Henry Martyn Robert, containing rules of order intended to be adopted as a parliamentary authority for use by a deliberative
    											assembly. As such, it is a guide for conducting meetings.</p>
    											<form method="get" action="/documents/roberts.pdf">
    												<button class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear" type="submit">Download!<i class="fa fa-arrow-right pl-10"></i></button>
    											</form>
    										</div>
										</div>
									</div>
								</div>
								