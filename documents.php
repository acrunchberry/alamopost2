<?php

include("header.php");
?>
<div id="page-start"></div>
			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="home.php">Home</a></li>
						<li class="active">Post Documents</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->
			
            <!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">
			<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">

							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Alamo Post 2 Public Documents</h1>
							<div class="separator-2"></div>
							<!-- page-title end -->
							<p class="lead">
							    The following documents are made publicly available to our members for personal use and reference.
							    All documents are current versions and can be downloaded and printed.
							</p>		    
				            <!-- Start Halls Grid -->
				            
				            <?php
				                include("document-grid.php");
				            ?>
						</div>
                    </div>
                </div>
        	</section>	

<!-- main-container end -->



<?php
include("footer.php");
include("jscripts.php");
?>
